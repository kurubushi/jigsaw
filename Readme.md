# jigsaw puzzle checker

Enjoy ksklab's jigsaw puzzle!


## compile

I have checked with OCaml version 4.07.0.

depends on oasis, ppx_deriving and menhir.

```
$ opam switch 4.07.0
$ opam install oasis ppx_deriving menhir
$ oasis setup
$ make
```


## example of use

```
$ ./checker.byte < test/jigsaw.tc
```

If you write a line: `print_DOT f : i -> o => mytest` in test/jigsaw.tc, checker creates DOT files `mytest_{trans/input/output/checker}.dot` in current directory.
`dot2pdf` converts all dot file like this:

```
$ ./dot2pdf mytest
# => creates mytest_{trans/input/output/checker}.pdf
```

`dot2pdf` depends on graphviz.
