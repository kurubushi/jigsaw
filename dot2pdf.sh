#!/usr/bin/env bash

## dot -> pdf

for x in trans input output checker
do
    dot -T pdf "$1"_$x.dot -o "$1"_$x.pdf
done
