{
  open Parser
}

let ws = [' ' '\t' '\n' '\r']
let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']
let alphanum = lower | upper | digit
let symbol = alphanum | '_' | '\'' | '*' | '+' | '(' | ')' | '|'
let var = symbol+


rule token = parse
  | ws+ { token lexbuf }
  | "//" [^'\n']* { token lexbuf }
  | "->" { ARROW }
  | "=>" { DARROW }
  | "=" { EQUAL }
  | "{" { LBRACE }
  | "}" { RBRACE }
  | "[" { LBRACKET }
  | "]" { RBRACKET }
  | ":" { COLON }
  | "," { COMMA }
  | "-" { MINUS }
  | "/" { SLASH }
  | eof { EOF }
  | "automaton" { AUTOMATON }
  | "transducer" { TRANSDUCER }
  | "initial" { INITIAL }
  | "accepted" { ACCEPTED }
  | "rules" { RULES }
  | "typecheck" { TYPECHECK }
  | "print_DOT" { PRINTDOT }
  | var { VAR (Lexing.lexeme lexbuf) }

(*
automaton num = {
  initial  : [q0],
  accepted : [q2,q1],
  rules : {
    q0 -0-> q1,
    q1 -0-> q2
    q2 -0-> q2
  }
}

transducer f = {
  initial  : [q0],
  accepted : [q2,q1],
  rules : {
    q0 -0/1-> q1,
    q1 -0/1-> q2
    q2 -0/1-> q2
  }
}

typecheck f: num -> num
*)
