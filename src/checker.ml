let print_string_list sts =
  let rec go = function
    | []     -> ()
    | [st]   -> Printf.printf "%s" st
    | st::sts -> Printf.printf "%s," st; go sts
  in
  go sts

let print_to_file (filename : string) (contents : string) =
  let oc = open_out filename in
  Printf.fprintf oc "%s" contents;
  close_out oc

let () =
  let lexbuf = Lexing.from_channel stdin in
  let phrases = Parser.main Lexer.token lexbuf in
  let a_hashs, t_hashs, ts, ps = Ast.ast2info phrases in
  let taux (t : Ast.typecheck)  =
    let trans = Hashtbl.find t_hashs t.transducer in
    let input = Hashtbl.find a_hashs t.input in
    let output = Hashtbl.find a_hashs t.output in
    let result = Typecheck.typecheck trans.info input.info output.info in
    Printf.printf "%s: %s -> %s :" t.transducer t.input t.output;
    match result with
    | NotSubSet (iw,ow) ->
       Printf.printf "false !\n  counter example is %s(" t.transducer;
       print_string_list iw;
       Printf.printf ") -> ";
       print_string_list ow;
       Printf.printf "\n"
    | SubSet ->
       Printf.printf "true !\n"
  in
  let paux (p : Ast.print_dot)  =
    let trans = Hashtbl.find t_hashs p.transducer in
    let input = Hashtbl.find a_hashs p.input in
    let output = Hashtbl.find a_hashs p.output in
    let dir_name = p.dir_name in
    print_to_file (dir_name ^ "_output.dot") (Typecheck.automaton_to_dot (output.info.initials,output.transes,output.info.accepteds));
    print_to_file (dir_name ^ "_input.dot") (Typecheck.automaton_to_dot (input.info.initials,input.transes,input.info.accepteds));
    print_to_file (dir_name ^ "_trans.dot") (Typecheck.automaton_to_dot (trans.info.initials,trans.transes,trans.info.accepteds));
    let memo = Typecheck.make_checker_automaton trans.info input.info output.info in
    print_to_file (dir_name ^ "_checker.dot") (Typecheck.memo_to_dot memo);
    Printf.printf "print DOT: %s: %s -> %s => %s_{trans/input/output/checker}.dot\n" p.transducer p.input p.output p.dir_name
  in
  List.iter taux ts;
  List.iter paux ps;
  flush stdout
