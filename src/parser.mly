%{
   open Ast
   open Typecheck
%}

%token ARROW DARROW
%token EQUAL
%token LBRACE RBRACE
%token LBRACKET RBRACKET
%token COLON
%token COMMA
%token MINUS
%token SLASH
%token EOF

%token AUTOMATON TRANSDUCER INITIAL ACCEPTED RULES TYPECHECK PRINTDOT
%token <string> VAR

%start main
%type <Ast.phrase list> main
%type <Ast.phrase list> phrases

%%

main:
| phrases EOF { $1 }
;

phrases:
| { [] }
| phrases automaton  { $2 :: $1 }
| phrases transducer { $2 :: $1 }
| phrases typecheck  { $2 :: $1 }
| phrases print_dot  { $2 :: $1 }
;

automaton:
| AUTOMATON VAR EQUAL LBRACE initials COMMA accepteds COMMA rules RBRACE
    { Automaton { name      = $2;
                  initials  = $5;
                  accepteds = $7;
                  rules     = $9 }
    }
;

transducer:
| TRANSDUCER VAR EQUAL LBRACE initials COMMA accepteds COMMA trules RBRACE
    { Transducer { name      = $2;
                   initials  = $5;
                   accepteds = $7;
                   trules    = $9 }
    }
;

initials:
| INITIAL COLON LBRACKET RBRACKET { [] }
| INITIAL COLON LBRACKET states RBRACKET { $4 }
;

accepteds:
| ACCEPTED COLON LBRACKET RBRACKET { [] }
| ACCEPTED COLON LBRACKET states RBRACKET { $4 }
;

states:
| states COMMA state { $3 :: $1 }
| state { [$1] }
;
state:
| VAR { {name = $1} }   
;

rules:
| RULES COLON LBRACKET RBRACKET { [] }
| RULES COLON LBRACKET rules_ RBRACKET { $4 }
;
rules_:
| rules_ COMMA state MINUS VAR ARROW state { ($3,$5,$7) :: $1 }
| state MINUS VAR ARROW state { [($1,$3,$5)] }
;

trules:
| RULES COLON LBRACKET RBRACKET { [] }
| RULES COLON LBRACKET trules_ RBRACKET { $4 }
;
trules_:
| trules_ COMMA state MINUS VAR SLASH word ARROW state { ($3,$5,$7,$9) :: $1 }
| state MINUS VAR SLASH word ARROW state { [($1,$3,$5,$7)] }
;

word:
| LBRACKET RBRACKET { [] }
| LBRACKET vars RBRACKET { List.rev $2 }
;

(* reversed *)
vars:
| VAR { [$1] }
| vars COMMA VAR { $3::$1 }
;

typecheck:
| TYPECHECK VAR COLON VAR ARROW VAR
    { Typecheck { transducer = $2;
                  input      = $4;
                  output     = $6 }
    }
;

print_dot:
| PRINTDOT VAR COLON VAR ARROW VAR DARROW VAR
    { Print_dot { transducer = $2;
                  input      = $4;
                  output     = $6;
                  dir_name   = $8; }
    }
;
