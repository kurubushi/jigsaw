type name = string
type state = Typecheck.state
type symbol = Typecheck.symbol
type word = Typecheck.symbol list
type rule = (state * symbol * state)
type trule = (state * symbol * word * state)

type automaton =
  { info    : Typecheck.automaton;
    transes : (state*symbol*state) list }
type transducer =
  { info    : Typecheck.transducer;
    transes : (state*symbol*state) list }
type automaton_hashs = (name, automaton) Hashtbl.t
type transducer_hashs = (name, transducer) Hashtbl.t
type typecheck = { transducer : name;
                   input      : name;
                   output     : name }
type print_dot = { transducer : name;
                   input      : name;
                   output     : name;
                   dir_name   : name }
type phrase =
  | Automaton of { name      : name;
                   initials  : state list;
                   accepteds : state list;
                   rules     : rule list }
  | Transducer of { name      : name;
                    initials  : state list;
                    accepteds : state list;
                    trules    : trule list }
  | Typecheck of typecheck
  | Print_dot of print_dot


let make_rules rules : Typecheck.automaton_hash =
  let a_hash = Hashtbl.create 32 in
  List.iter (fun (q,s,p) -> Hashtbl.add a_hash (q,s) p) rules;
  a_hash

let make_trules trules : Typecheck.transducer_hash =
  let t_hash = Hashtbl.create 32 in
  List.iter (fun (q,a,b,p) -> Hashtbl.add t_hash (q,b) (p,a)) trules;
  t_hash

let ast2info (phrases : phrase list) : (automaton_hashs * transducer_hashs * typecheck list * print_dot list) =
  let a_hashs : automaton_hashs = Hashtbl.create 8 in
  let t_hashs : transducer_hashs = Hashtbl.create 8 in
  let aux (ts,ps : typecheck list * print_dot list) = function
    | Automaton i ->
       let trs = List.fold_left
                  (fun trs (q,a,p) -> (q,a,p)::trs) [] i.rules in
       let a : Typecheck.automaton =
         { name = i.name;
           rules = make_rules i.rules;
           initials = i.initials;
           accepteds = i.accepteds }
       in
       Hashtbl.add a_hashs i.name { info = a; transes = trs };
       ts,ps
    | Transducer i ->
       let oa,trs = List.fold_left (fun (oa,trs) (q,a,b,p) ->
                       if List.mem b oa then
                         (oa,(q,a^"/"^(Typecheck.show_word b),p)::trs)
                       else
                         (b::oa,(q,a^"/"^(Typecheck.show_word b),p)::trs)) ([],[]) i.trules in
       let t : Typecheck.transducer =
         { name = i.name;
           rules = make_trules i.trules;
           initials = i.initials;
           accepteds = i.accepteds;
           outputw_alphabet = oa }
       in
       Hashtbl.add t_hashs i.name {info = t; transes = trs };
       ts,ps
    | Typecheck t ->
       t::ts,ps
    | Print_dot p ->
       ts,p::ps
  in
  let ts,ps = List.fold_left aux ([],[]) phrases in
  (a_hashs, t_hashs, ts, ps)
