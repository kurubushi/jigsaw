module State = struct
  type t = { name : string } [@@deriving show]
  let compare (p1 : t) (p2 : t) : int =
    String.compare p1.name p2.name
  let show (p : t) : string =
    p.name
end

module PState = struct (* Pair State *)
  type t = State.t * State.t [@@deriving show]
  let compare ((p11,p12) : t) ((p21,p22) : t) : int =
    match State.compare p11 p21 with
    | 0 -> State.compare p12 p22
    | i -> i
end

type state = State.t [@@deriving show]
type states = state list [@@deriving show]
type symbol = string [@@deriving show]
type word = symbol list [@@deriving show]
type alphabet = symbol list [@@deriving show]
type ssw = state * state * word [@@deriving show]
type ssws = ssw list [@@deriving show]

type transducer_hash = ((state*word),(state*symbol)) Hashtbl.t
type automaton_hash  = ((state*symbol),state) Hashtbl.t

type transducer =
  { name      : string;
    rules     : transducer_hash;
    initials  : state list;
    accepteds : state list;
    outputw_alphabet : word list }
type automaton =
  { name      : string;
    rules     : automaton_hash;
    initials  : state list;
    accepteds : state list }

(* trace memo *)
type memo_key = ((state*state) list) * (state list)
type memo_status = { is_new : bool }
type memo = (memo_key, memo_status) Hashtbl.t

type typecheck_result =
  SubSet
| NotSubSet of word * word (* counter input/output example *)

(* a remaining task must be traced *)
(* without input_word tracing
type queue_item =        (* f(... : p) -> output_word : q *)
  ((state * state) list) (* [(f,q)] *)
  * state list           (* [p] *)
  * word                 (* output_word *)
*) 
type queue_item =  (* f(input_word : p) -> output_word : q *)
    ssw list       (* [f,q,reversed input_word] *)
  * state list     (* [p] *)
  * word           (* output_word *)
type queue = queue_item Queue.t

type check_result =
  Skip (* necessary? *)
| Stop of { counter_input : word;
            fqws          : ssw list;
            ps            : state list }
| Continue of { fqws : ssw list;
                ps   : state list }

type env =
  { init_item       : queue_item;
    trans           : transducer;
    input           : automaton;
    output          : automaton;
    outputw_alphabet : word list;
    memo            : memo }




let const x _ = x

let cartesian l l' = 
  List.concat (List.map (fun e -> List.map (fun e' -> (e,e')) l') l)


let normalform (fqws : ssws) (ps : states) : ((state*state) list * state list) =
  let fqs = List.map (fun (f,q,w) -> (f,q)) fqws in
  let fqs' = List.sort_uniq PState.compare fqs in
  let ps' = List.sort_uniq State.compare ps in
  (fqs',ps')

let arrive (env : env) (fqws : ssw list) (ps : State.t list) =
  let fqs',ps' = normalform fqws ps in
  Hashtbl.replace env.memo (fqs',ps') { is_new = true }

let is_arrived (env : env) (fqws : ssw list) (ps : State.t list) =
  let fqs',ps' = normalform fqws ps in
  Hashtbl.mem env.memo (fqs',ps')

let next_fqws (env : env) (o : word) (fqws : ssw list) =
  let next_fqw (f,q,w : ssw) =
    let fis = Hashtbl.find_all env.trans.rules (f,o) in
    let qs_by i = Hashtbl.find_all env.input.rules (q,i) in
    let with_fi (f,i) qs = List.map (fun q -> (f,q,i::w)) qs in
    let fqws' = List.concat (List.map (fun (f,i) -> with_fi (f,i) (qs_by i)) fis) in 
    fqws'
  in
  List.concat (List.map next_fqw fqws)
  
let rec next_ps (env : env) (s : word) (ps : state list) =
  match s with
  | []     -> ps
  | (o::s) ->
     let ps' = List.concat (List.map (fun p -> Hashtbl.find_all env.output.rules (p,o)) ps) in
     next_ps env s ps'

let is_accepted_fqw (t : transducer) (a : automaton) ((f,q,_) : ssw) =
  List.mem f t.accepteds && List.mem q a.accepteds
let is_initial_fqw (t : transducer) (a : automaton) ((f,q,_) : ssw) =
  List.mem f t.initials && List.mem q a.initials
let is_accepted_p (a : automaton) (p : state) =
  List.mem p a.accepteds
let is_initial_p (a : automaton) (p : state) =
  List.mem p a.initials

let typecheck_by (env : env) (s : word) (fqws,ps : (ssw list) * (state list)) : check_result =
  let fqws' = next_fqws env s fqws in
  let ps' = next_ps env s ps in
  let fqws'' = List.filter (is_accepted_fqw env.trans env.input) fqws' in
  let ps'' = List.filter (is_accepted_p env.output) ps' in
  match fqws'',ps'' with
  | (_,_,w)::_,[] -> (* fqws'' only accepted *)
     Stop { counter_input = w; fqws = fqws'; ps = ps'}
  | _, _  -> (* next newer states *)
     Continue { fqws = fqws'; ps = ps' }


let bfs (solver : 'a -> env -> queue -> 'b) (trans : transducer) (input : automaton) (output : automaton) : 'a -> 'b = fun x ->
  let fqs = cartesian trans.initials input.initials in
  let fqws = List.map (fun (f,q) -> (f,q,[])) fqs in
  let ps = output.initials in
  let env =
    { init_item = fqws,ps,[];
      trans = trans;
      input = input;
      output = output;
      outputw_alphabet = trans.outputw_alphabet;
      memo = Hashtbl.create 1024 } in
  let result = begin
      let pool : queue = Queue.create () in
      Queue.push env.init_item pool;
      solver x env pool
    end
  in
  result

let rec typecheck_loop (env : env) (pool : queue) : typecheck_result =
  if Queue.is_empty pool then
    SubSet
  else
    let (fqws,ps,st) = Queue.pop pool in
    let rec go = function
      | s::ss -> begin
          match typecheck_by env s (fqws,ps) with
          | Skip -> (* necessary? *)
             go ss
          | Continue r -> (* new task must be traced *)
             (* Printf.printf "Continue with\n  fqws: %s\n  ps: %s\n  st:%s\n" (show_ssws r.fqws) (show_states r.ps) (show_word (s::st)); *)
             Queue.push (r.fqws,r.ps, (List.append st s)) pool;
             go ss
          | Stop r -> (* counter example is founded *)
             (* Printf.printf "Stop at\n  fqws: %s\n  ps: %s\n  st:%s\n" (show_ssws fqws) (show_states ps) (show_word (s::st)); *)
             NotSubSet (List.rev r.counter_input, (List.append st s))
        end
      | []    -> typecheck_loop env pool
    in
    if is_arrived env fqws ps then (* not have to trace *)
      typecheck_loop env pool
    else begin (* have to trace *)
        arrive env fqws ps;
        go env.outputw_alphabet
      end
  
let typecheck (trans : transducer) (input : automaton) (output : automaton) : typecheck_result =
  bfs (const typecheck_loop) trans input output ()


let rec make_checker_automaton_loop (accepted_list,trans_list : (memo_key) list * (memo_key*word*memo_key) list) (env : env) (pool : queue) : (memo_key * (memo_key * word * memo_key) list * memo_key list) =
  if Queue.is_empty pool then
    let (fqws,ps,_) = env.init_item in
    (normalform fqws ps, trans_list, accepted_list)
  else
    let (fqws,ps,st) = Queue.pop pool in
    let phase = normalform fqws ps in
    let rec go accepteds transes = function
      | s::ss -> begin
          match typecheck_by env s (fqws,ps) with
          | Skip -> (* necessary? *)
             go accepteds transes ss
          | Continue r -> (* new task must be traced *)
             let phase' = normalform r.fqws r.ps in
             let transes' = (phase, s, phase') :: transes in
             Queue.push (r.fqws,r.ps, (List.append st s)) pool;
             go accepteds transes' ss
          | Stop r -> (* counter example is founded *)
             let phase' = normalform r.fqws r.ps in
             let transes' = (phase, s, phase') :: transes in
             let accepteds' = phase' :: accepteds in
             Queue.push (r.fqws,r.ps, (List.append st s)) pool;
             go accepteds' transes' ss
        end
      | []    -> make_checker_automaton_loop (accepteds,transes) env pool
    in
    if is_arrived env fqws ps then (* not have to trace *) 
      make_checker_automaton_loop (accepted_list,trans_list) env pool
    else begin (* have to trace *)
        arrive env fqws ps;
        go accepted_list trans_list env.outputw_alphabet
      end

let make_checker_automaton (trans : transducer) (input : automaton) (output : automaton) : memo_key * (memo_key*word*memo_key) list * memo_key list =
  bfs make_checker_automaton_loop trans input output ([],[])

let show_fq ((f,q) : state*state) : string =
  Printf.sprintf "%s(%s)" f.name q.name

let show_p (p : state) : string =
  Printf.sprintf "%s" p.name

let rec show_fqs (fqs : (state*state) list) : string =
  match fqs with
  | [fq] -> show_fq fq
  | fq::fqs' -> show_fq fq ^ "," ^ show_fqs fqs'
  | [] -> ""

let rec show_ps (ps : state list) : string =
  match ps with
  | [p] -> show_p p
  | p::ps' -> show_p p ^ "," ^ show_ps ps'
  | [] -> ""

let show_memo_key (fqs,ps : memo_key) : string =
  Printf.sprintf "\"[%s],[%s]\"" (show_fqs fqs) (show_ps ps)

let show_memo_key_list ks =
  List.fold_left (fun acc k -> acc ^ " " ^ show_memo_key k) "" ks

let show_word st =
  let rec go st = match st with
    | []      -> ""
    | (w::st) -> "," ^ w ^ go st
  in
  match st with
  | []      -> "[]"
  | (w::[]) -> "[" ^ w ^ "]"
  | (w::st) -> "[" ^ w ^ go st ^ "]"

let show_trans (k,s,k' : memo_key * word * memo_key) : string =
  Printf.sprintf "%s -> %s [label=\"%s\"];\n" (show_memo_key k) (show_memo_key k') (show_word s)

let memo_to_dot (init,trans,accepteds : memo_key * (memo_key*word*memo_key) list * memo_key list) =
  let r = "" in
  let r = r ^ Printf.sprintf "digraph G {\n" in
  let r = r ^ Printf.sprintf "rankdir=LR;\n" in
  let r = r ^ Printf.sprintf "staring_arrow_source [label=\"\" shape=plaintext peripheries=0]\n" in
  let r = r ^ Printf.sprintf "node [shape=ellipse peripheries=2]; %s\n" (show_memo_key_list accepteds) in
  let r = r ^ Printf.sprintf "node [shape=ellipse peripheries=1];\n" in
  let r = r ^ Printf.sprintf "staring_arrow_source -> %s\n" (show_memo_key init) in
  let r = r ^ List.fold_left (fun acc tr -> acc ^ show_trans tr) "" trans in
  let r = r ^ Printf.sprintf "}\n" in
  r

let automaton_to_dot (inits,trans,accepteds : state list * (state*symbol*state) list * state list) =
  let show_state p = Printf.sprintf "\"%s\"" (State.show p) in
  let rec show_states = function
    | [p] -> show_state p
    | p::ps -> show_state p ^ "," ^ show_states ps
    | [] -> ""
  in
  let show_trans (p,s,p') =
    Printf.sprintf "\"%s\" -> \"%s\" [label=\"%s\"];\n" (State.show p) (State.show p') s
  in
  let show_init p =
    let r = "" in
    let r = r ^ Printf.sprintf "\"staring_arrow_source_for_%s\" [label=\"\" shape=plaintext peripheries=0]\n" (State.show p) in
    let r = r ^ Printf.sprintf "\"staring_arrow_source_for_%s\" -> \"%s\"\n" (State.show p) (State.show p) in
    r
  in
  let r = "" in
  let r = r ^ Printf.sprintf "digraph G {\n" in
  let r = r ^ Printf.sprintf "rankdir=LR;\n" in
  let r = r ^ Printf.sprintf "node [shape=ellipse peripheries=2]; %s\n" (show_states accepteds) in
  let r = r ^ Printf.sprintf "node [shape=ellipse peripheries=1];\n" in
  let r = r ^ List.fold_left (fun acc p -> acc ^ show_init p) "" inits in
  let r = r ^ List.fold_left (fun acc tr -> acc ^ show_trans tr) "" trans in
  let r = r ^ Printf.sprintf "}\n" in
  r
